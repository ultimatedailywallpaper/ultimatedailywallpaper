![](src/icons/ultimatedesktopwallpaper_icon.png)

# UltimateDailyWallpaper
UltimateDailyWallpaper is a utility to use the daily picture of a provider as wallpaper on Linux and macOS operating systems. It saves images in a high quality. Further information about each picture can be obtained from the internet with a single click. It integrates with most desktop environments to provide automatically changing wallpaper. It allows a simple integration of external plugins, which allows downloading a daily wallpaper from any one provider.

## Features of the application:
 * Allows download the daily picture of Wikimedia Commons (POTD) in a high quality
 * Allows download "pictures of the day" from past days of Wikimedia Commons
 * Allows download the daily wallpaper of Bing in a high quality
 * Allows a simple integration of external plugins to download a daily picture of any provider
 * Supports the most of desktop environments to provide automatically changing wallpaper
 * Allows automatically changing wallpaper on lockscreen of KDE Plasma 5 and GNOME

## Recomment:
* gnome-shell-extension-appindicator - support for legacy tray icons on GNOME 3 -> https://github.com/ubuntu/gnome-shell-extension-appindicator
* gnome-shell-extension-top-icons-plus - https://github.com/phocean/TopIcons-plus

## Installation:

### macOS
Download the current release of UltimateDailyWallpaper for macOS: <br>
<a href="https://gitlab.com/ultimatedailywallpaper/ultimatedailywallpaper/-/releases">https://gitlab.com/ultimatedailywallpaper/ultimatedailywallpaper/-/releases</a>

### Linux
UltimateDailyWallpaper is currently available in the repositories of:
 * <a href="https://software.opensuse.org/package/UltimateDailyWallpaper?search_term=UltimateDailyWallpaper">OpenSUSE</a>
 * <a href="https://slackbuilds.org/repository/15.0/desktop/UltimateDailyWallpaper/">SlackBuilds.org</a>

#### Installation on OpenSUSE
```
sudo zypper install UltimateDailyWallpaper
```

#### Installation on Slackware 15+

Sbopkg is required to install UltimateDailyWallpaper on Slackware Linux.

```
sudo sbopkg -B -i UltimateDailyWallpaper
```

#### Installation on Fedora 38

```
wget https://gitlab.com/ultimatedailywallpaper/ultimatedailywallpaper/uploads/2f858cfeee92aefbbf55c56cc2894420/UltimateDailyWallpaper-3.3.6-1.fc38.x86_64.rpm
sudo rpm -i UltimateDailyWallpaper-3.3.6-1.fc38.x86_64.rpm
```

#### Installation on Debian 12+

```
wget https://gitlab.com/ultimatedailywallpaper/ultimatedailywallpaper/uploads/04b6fae5f6c62a66b16d4e77e13fbcc0/ultimatedailywallpaper_3.3.6-1_amd64.deb
sudo dpkg -i ultimatedailywallpaper_3.3.6-1_amd64.deb
```

## Build from source:
To build UltimateDailyWallpaper from source, you will need:

### Build requirements:
 * Qt 5.14+
 * Git
 * mesa-libGL
 * make
 * gcc/gcc-c++

### Required packages to use UltimateDailyWallpaper:
 * curl

### Build instructions:

```
$ git clone https://gitlab.com/ultimatedailywallpaper/ultimatedailywallpaper.git
$ cd ultimatedailywallpaper
$ qmake (or qmake-qt5)
$ make -f Makefile
```
## Screenshots: <br />

### Main Application
![](screenshots/newpicture_desktop_001.png)

### Application menu
![](screenshots/new_menu_001.png)

### Load stored picture
![](screenshots/load_existing_picture.png)
