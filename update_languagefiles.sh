#!/bin/bash

BASEPATH=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

lupdate ${BASEPATH}/src/src.pro
sed -i 's|'${BASEPATH}'/|../|g' ${BASEPATH}/language/*.po
sed -i 's|'${BASEPATH}'/|../|g' ${BASEPATH}/language/*.pot
