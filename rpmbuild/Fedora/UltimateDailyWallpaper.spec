Name:           UltimateDailyWallpaper
Version:        3.3.6
Release:        1%{?dist}
Summary:        A wallpaper changer and downloader
License:        GPLv3
URL:            https://gitlab.com/ultimatedailywallpaper/ultimatedailywallpaper
Source0:        %{url}/-/archive/%{version}/ultimatedailywallpaper-%{version}.tar.gz

BuildRequires:  libstdc++-devel
BuildRequires:  mesa-libGL-devel
BuildRequires:  qt5-qtbase-devel
BuildRequires:  qt5-qtbase-common
BuildRequires:  qt5-qttools-devel
BuildRequires:  qt5-qttranslations
BuildRequires:  qt5-qtdeclarative-devel
BuildRequires:  qtsinglecoreapplication-qt5-devel
BuildRequires:  qt5-qtbase-gui
BuildRequires:  qt5-qtwayland-devel
BuildRequires:  glibc-devel
BuildRequires:  make
BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  desktop-file-utils

Requires:       curl

%description
UltimateDailyWallpaper is a utility to set the daily
picture as wallpaper of a selected provider. It saves
images in a high quality. Further information about
each picture can be obtained from the Internet with a
single click. It integrates with most desktop
environments to provide automatically changing wallpaper.
It allows a simple integration of external plugins, which
allows downloading a daily wallpaper from any one provider.

%global debug_package %{nil}

%prep
rm -rf SlackBuild
rm -rf debian
rm -rf screenshots
rm -rf src/macOS
%setup -q -n ultimatedailywallpaper-%{version} -a 0

%build
export QTDIR="%{_qt5_prefix}"
export PATH="%{_qt5_bindir}:$PATH"
lrelease-qt5 src/src.pro
qmake-qt5
%make_build

%install
mkdir -p %{buildroot}/%{_bindir}
mkdir -p %{buildroot}/%{_datadir}/pixmaps
mkdir -p %{buildroot}/%{_datadir}/applications
mkdir -p %{buildroot}/%{_libdir}/%{name}-plugins
mkdir -p %{buildroot}/%{_datadir}/locale/ca/LC_MESSAGES
mkdir -p %{buildroot}/%{_datadir}/locale/de/LC_MESSAGES
mkdir -p %{buildroot}/%{_datadir}/locale/es/LC_MESSAGES
mkdir -p %{buildroot}/%{_datadir}/locale/fi/LC_MESSAGES
mkdir -p %{buildroot}/%{_datadir}/locale/fr/LC_MESSAGES
mkdir -p %{buildroot}/%{_datadir}/locale/it/LC_MESSAGES
mkdir -p %{buildroot}/%{_datadir}/locale/ru/LC_MESSAGES
mkdir -p %{buildroot}/%{_datadir}/locale/uk/LC_MESSAGES
mkdir -p %{buildroot}/%{_datadir}/locale/ko/LC_MESSAGES
mkdir -p %{buildroot}/%{_datadir}/locale/pt/LC_MESSAGES
cp language/%{name}_ca.qm %{buildroot}/%{_datadir}/locale/ca/LC_MESSAGES
cp language/%{name}_de.qm %{buildroot}/%{_datadir}/locale/de/LC_MESSAGES
cp language/%{name}_es.qm %{buildroot}/%{_datadir}/locale/es/LC_MESSAGES
cp language/%{name}_fi.qm %{buildroot}/%{_datadir}/locale/fi/LC_MESSAGES
cp language/%{name}_fr.qm %{buildroot}/%{_datadir}/locale/fr/LC_MESSAGES
cp language/%{name}_it.qm %{buildroot}/%{_datadir}/locale/it/LC_MESSAGES
cp language/%{name}_ru.qm %{buildroot}/%{_datadir}/locale/ru/LC_MESSAGES
cp language/%{name}_uk.qm %{buildroot}/%{_datadir}/locale/uk/LC_MESSAGES
cp language/%{name}_ko.qm %{buildroot}/%{_datadir}/locale/ko/LC_MESSAGES
cp language/%{name}_pt.qm %{buildroot}/%{_datadir}/locale/pt/LC_MESSAGES
cp src/icons/ultimatedesktopwallpaper_icon.png %{buildroot}/%{_datadir}/pixmaps
cp %{name}.desktop %{buildroot}/%{_datadir}/applications
install -m 0755 bin/%{name} %{buildroot}/%{_bindir}/%{name}
install -m 0755 plugins/libbing-wallpaper-downloader-*.so %{buildroot}/%{_libdir}/%{name}-plugins
install -m 0755 plugins/libwikimedia-commons-potd-*.so %{buildroot}/%{_libdir}/%{name}-plugins
%find_lang %{name} --with-qt

%check
desktop-file-validate %{buildroot}%{_datadir}/applications/*.desktop

%files -f %{name}.lang
%dir %{_libdir}/%{name}-plugins
%{_bindir}/%{name}
%{_datadir}/pixmaps/ultimatedesktopwallpaper_icon.png
%{_libdir}/%{name}-plugins/libbing-wallpaper-downloader-*.so
%{_libdir}/%{name}-plugins/libwikimedia-commons-potd-*.so
%{_datadir}/applications/%{name}.desktop
%license LICENSE
%doc README.md AUTHORS

%changelog
* Thu Aug 27 2023 Patrice Coni <patrice.coni-dev@yandex.com> - 3.3.6-1
- Update to version 3.3.6
   * about.cpp line 101: URL updated

* Thu Aug 03 2023 Patrice Coni <patrice.coni-dev@yandex.com> - 3.3.5-1
- Update to version 3.3.5
   * Bug fixed: Application crashes when trying to delete
		outdated pictures.
   * Language files updated

* Thu Aug 03 2023 Patrice Coni <patrice.coni-dev@yandex.com> - 3.3.4-1
- Update to version 3.3.4
   * Bug fixed: Wallpaper changes undesirably after
                reconnecting to the internet
   * Language files updated

* Tue Aug 01 2023 Patrice Coni <patrice.coni-dev@yandex.com> - 3.3.3-1
- Update to version 3.3.3
   * Bug fixed: The wallpaper does not change after
                the internet connection was reconnected
   * Cancel button in Settings user interface selected as default
   * Language files updated

* Mon Jul 31 2023 Patrice Coni <patrice.coni-dev@yandex.com> - 3.3.2-1
- Initial release
